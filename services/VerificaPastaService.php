<?php
require "ExtraiPastaService.php";

class VerificaPastaService {

  private $classname;

  public function __construct($classname){
    $this->classname = $classname;
  }

  public function verifica(){

    $tipo = new ExtraiPastaService($this->classname);
    return is_dir($tipo->pasta()) ? true : false;

  }
}
