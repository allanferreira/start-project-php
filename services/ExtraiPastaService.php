<?php
class ExtraiPastaService {
  private $classname;
  public function __construct($classname){
    $this->classname = $classname;
  }
  public function pasta(){

      $arrLetras = str_split($this->classname);
      $invertArrLetras = array_reverse($arrLetras);
      $arrQuebradoInvertido = [];

      foreach ($invertArrLetras as $key => $letra) {
        if(ctype_upper($letra)){

          $chaveDeQuebra = $key + 1;
          $arrQuebradoInvertido[] = array_slice($invertArrLetras, 0, $chaveDeQuebra);

        }
      }

      $arrQuebrado = array_reverse($arrQuebradoInvertido[0]);

      $tipoClasse = implode("",$arrQuebrado);
      $pasta = strtolower($tipoClasse).'s';

      return $pasta;
    }

}
