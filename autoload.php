<?php
require_once "services/VerificaPastaService.php";
require_once "services/ExtraiPastaService.php";

spl_autoload_register(function($classname) {

    $pastaExiste = new VerificaPastaService($classname);
    if($pastaExiste->verifica()){

      $pasta = new ExtraiPastaService($classname);
      include "{$pasta->pasta()}/$classname.php";

    }

});
