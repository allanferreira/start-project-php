<?php
require_once "vendor/autoload.php";

class Router {
  public function page() {

    Flight::route('GET /', ['HomeController','render']);

    Flight::route('GET /sobre', ['SobreController','render']);

    Flight::route('*', function(){
      echo '<h3>404</h3>';
      echo '<a href="/">Voltar para a Home</a><br>';
    });

    Flight::start();

  }
}
