<?php
require_once "vendor/autoload.php";
require_once "autoload.php";

class HomeController extends PaginaController {
  public static function render(){
    Flight::render('includes/header', [], 'header');
    Flight::render('includes/footer', [], 'footer');
    Flight::render('pages/home');
  }
}
